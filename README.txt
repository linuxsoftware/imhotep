Imhotep Scaffold for Pyramid development
========================================
`Pyramid <http://docs.pylonsproject.org/docs/pyramid.html>`_ is my Python web
framework of choice.  It is easy to get started, has lots of features, but
doesn't force them on you, and so gives lots of freedom.


Installation
------------
::

    # Prepare isolated environment
    PYRAMID_ENV=$HOME/Projects/pyramid-env
    virtualenv $PYRAMID_ENV 
    # Activate isolated environment
    source $PYRAMID_ENV/bin/activate
    # Install packages
    pip install --upgrade ipython ipdb imhotep


Usage
-----
::

    # Activate isolated environment
    source $PYRAMID_ENV/bin/activate
    # Enter workspace
    PROJECTS=$HOME/Projects
    cd $PROJECTS
    # List available scaffolds
    pcreate --list-templates
    # Create an application
    pcreate -t imhotep example
