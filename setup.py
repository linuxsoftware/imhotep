#!/bin/env python
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = ['pyramid', 
            'shortuuid',
           ]

setup(name='imhotep',
      version='0.0.1',
      description='Imhotep Scaffold for Pyramid development',
      long_description=README + '\n\n' + CHANGES,
      classifiers=["Development Status :: 2 - Pre-Alpha",
                   "Environment :: Web Environment",
                   "Framework :: Pyramid",
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: MIT License',
                   "Operating System :: POSIX :: Linux",
                   'Programming Language :: Python',
                   "Topic :: Software Development :: Code Generators",
                  ],
      platforms=['Linux'],
      author='David Moore',
      author_email='david@linuxsoftware.co.nz',
      url='www.linuxsoftware.co.nz',
      license='MIT',
      keywords='web wsgi pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      entry_points="""\
      [pyramid.scaffold]
      imhotep_sql = imhotep.scaffolds:ImhotepSqlTemplate
      """,
     )
