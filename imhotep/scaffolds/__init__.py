from pyramid.scaffolds import PyramidTemplate
from shortuuid import ShortUUID

class ImhotepTemplate(PyramidTemplate):
    def pre(self, command, output_dir, vars): # pragma: no cover
        """
        Called before template is applied.
        """
        if 'password' not in vars:
            secrets = ShortUUID()
            vars['password'] = []
            for i in range(10):
                vars['password'].append(secrets.uuid())
        return PyramidTemplate.pre(self, command, output_dir, vars)

    def post(self, command, output_dir, vars): # pragma: no cover
        """
        Called after template is applied.
        """
        self.out("Another Pyramid by Imhotep")
        return PyramidTemplate.post(self, command, output_dir, vars)

class ImhotepSqlTemplate(ImhotepTemplate):
    _template_dir = 'imhotep_sql'
    summary = 'Imhotep-class of Pyramid SQLAlchemy project'

class ImhotepZodbTemplate(ImhotepTemplate):
    _template_dir = 'imhotep_zodb'
    summary = 'Imhotep-class of Pyramid Traversal/ZODB project'




