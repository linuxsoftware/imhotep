function initIdleTimeOut(inactiveTimeoutSecs)
{
    var inactiveTimeout = inactiveTimeoutSecs * 1000;
    var warningTimeout  = 10000;
    var warningTimer    = null;

    $(document).on("idle.idleTimer", function(event, elem, obj) {
        //Get time when user was last active
        var diff = (+new Date()) - obj.lastActive - obj.timeout;
        var warning = (+new Date()) - diff;
        
        // On mobile js is paused, so see if this was triggered 
        // while we were sleeping
        if (diff >= warningTimeout || warning <= 5000) {
            document.location.href = "/logout";
        } else {
            //Show dialog, and note the time
            $("#overlay").show();
            $("#time-out-warning").show();
            var remaining = Math.round((warningTimeout - diff) / 1000);
            $("#time-out-dialog .secsRemaining").html(remaining);
            var warningStart = (+new Date()) - diff;

            // Update counter downer every second
            warningTimer = setInterval(function () {
                var remaining = Math.round((warningTimeout / 1000) - 
                                           (((+new Date()) - warningStart) / 1000));
                if (remaining >= 0) {
                    $("#time-out-dialog .secsRemaining").html(remaining);
                } else {
                    document.location.href = "/logout";
                }
            }, 1000)
        }
    });

    // create a timer to keep server session alive, independent of idle timer
    setInterval(function () {
        $.ajax({'url': "/keepalive.json"});
    }, 900000);  // this needs to be less than the session timeout (currently 1800secs)

    //User clicked to extend session
    $("#overlay").click(hideTimeOutWarning);
    $("#extendSession").click(hideTimeOutWarning);
    $("#time-out-warning").click(hideTimeOutWarning);
    $("#time-out-dialog").click(function() { return false; } );
    $("#time-out-dialog .close").click(hideTimeOutWarning);

    function hideTimeOutWarning()
    {
        clearTimeout(warningTimer);
        $("#time-out-warning").hide();
        $("#overlay").hide();
        return false;
    }

    //User clicked logout
    $("#logoutSession").click(function () {
        document.location.href = "/logout";
    });

    //Set up the timer, if inactive for 10 seconds log them out
    $(document).idleTimer(inactiveTimeout);
}
