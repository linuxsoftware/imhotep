function ajajTable(tableName)
{
    var tbl = $('table.data').dataTable({
        'bAutoWidth':      false,
        'bFilter':         false,
        'bInfo':           false,
        'bLengthChange':   false,
        'iDisplayLength':  20,
        'bSort':           false,
        'bServerSide':     true,
        'sPaginationType': 'full_numbers',
        'sAjaxSource':     '/table.json',
        'fnServerParams':  tableServerParamsFunction(tableName),
    });
    $("input#delBtn").click(function () {
        return confirm("Are you sure you want to delete this row?");
    });
    $("table.data tbody").on("click", "tr", function () {
        $(this).siblings("tr").removeClass('row_selected');
        $(this).addClass('row_selected');
        tds = $(this).find("td");
        $("input#rowid").val(tds.eq(0).text());
    });
    $("table.data tbody").on("dblclick", "tr", function () {
        tds = $(this).find("td");
        document.location.href="/admin/"+tableName+"/"+tds.eq(0).text();
    });
}

function tableServerParamsFunction(tableName)
{
    var csrfToken = $("input#csrfToken").val();
    return function(aoData) {
        aoData.push({"name": "csrf_token", "value": csrfToken});
        aoData.push({"name": "tablename", "value": tableName});
    }
}

