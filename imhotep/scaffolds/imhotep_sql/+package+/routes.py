# ------------------------------------------------------------------------------
# All the routes are defined in this module
# ------------------------------------------------------------------------------
import logging
log = logging.getLogger(__name__)


def includeme(config):
    """Set up the routes"""
    config.add_route('home',              '/')
    config.add_route('login',             '/login')
    config.add_route('forgotpwd',         '/resetpwd')
    config.add_route('emailsent',         '/emailsent')
    config.add_route('resetpwd',          '/resetpwd/{token}')
    config.add_route('logout',            '/logout')
    config.add_route('user',              '/user')
    config.add_route('changepwd',         '/user/changepwd')
    config.add_route('setgauth',          '/user/setgauth')
    config.add_route('gauth.png',         '/user/gauth.png')
    config.add_route('keepalive.json',    '/keepalive.json',          xhr=True)

    log.info("routes added")

