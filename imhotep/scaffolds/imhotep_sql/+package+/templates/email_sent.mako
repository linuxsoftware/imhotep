<%inherit file="main.mako" />

<div id="content">
  <div id="reset-password">
    <h2>Email Sent:</h2>
    <form method="POST" action="${request.path_url}" enctype="multipart/form-data">
      <div class="explain-resetpwd">
        <p>
        An email has been sent to your email account, you should have it soon.
        (Also check it hasn't ended up in your spam folder.)
        It says what your login is and explains how to reset your password.
        </p>
      </div>

      <div class="tool-bar">
        ${btns.okBtn}
      </div>
    </form>
  </div>
</div>
