<%inherit file="main.mako" />

<div id="content">
  <div id="user-settings">
    <h2>Change Password:</h2>
    <form method="POST" action="${request.path_url}"
     enctype="multipart/form-data">
      %if form.errors:
        <ul class="errors">
          %for field, errors in form.errors.items():
              %for error in errors:
                  <li>${form[field].label}: ${error}</li>
              %endfor
          %endfor
        </ul>
      %endif
      <div class="password">
        %for field in form:
            <div class="field">
              ${field.label(class_="is-required")} ${field(maxlength=200)}
            </div>
        %endfor
      </div>
      <div class="tool-bar">
        ${btns.csrfToken}
        ${btns.hiddenBtn}
        ${btns.cancelBtn}
        ${btns.okBtn}
      </div>
    </form>
  </div>
  <div class="is-required-key">
    <span class="red-star">*</span> = is required
  </div>
</div>
