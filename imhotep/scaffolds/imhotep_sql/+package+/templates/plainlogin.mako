<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>Login</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="robots" content="noindex,nofollow,noarchive" />
  </head>
  <body>
    <h1>Login</h1>
    <form action="${url}" method="post">
      <div>${message}</div>
      <div class="login">
        <input type="hidden" name="came_from" value="${came_from}"/>
        <input type="text" name="login" value="${login}"/><br/>
        <input type="password" name="password"
               value="${password}"/><br/>
      </div>
      <div class="tool-bar">
        <input type="submit" name="form.submitted" value="Log Me In"/>
      </div>
    </form>
  </body>
</html>
