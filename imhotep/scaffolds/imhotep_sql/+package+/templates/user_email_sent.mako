<%inherit file="main.mako" />

<div id="content">
  <div id="reset-password">
    <h2>Email Sent:</h2>
    <form method="POST" action="${request.path_url}" enctype="multipart/form-data">
      <div class="explain-resetpwd">
        <p>
        An email has been sent to their email account, they should have it soon.
        It will explain to them how to reset their password.
        </p>
      </div>

      <div class="tool-bar">
        ${btns.okBtn}
      </div>
    </form>
  </div>
</div>
