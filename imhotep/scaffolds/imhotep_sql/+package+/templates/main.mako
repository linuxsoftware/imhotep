<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    ${self.head()}
    ${self.javascript()}
  </head>
  <body>
    <div id="wrapper">
      ${self.background()}
      ${self.topBar()}
      ${next.body()}
      ${self.overlay()}
      ${self.bottomBar()}
      ${self.timeOutWarning()}
    </div> <!-- wrapper -->
  </body>
</html>

<%!
from pyramid.security import has_permission
%>

<%def name="head()">
<title>{{package}}</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <meta name="robots" content="noindex,nofollow,noarchive" />
  <meta name="viewport" content="width=device-width">
  <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico"/>
  <link rel="stylesheet" href="/static/{{package}}.css?${request.revstr}"
        type="text/css" media="screen" charset="utf-8" />
</%def>

<%def name="topBar()">
  <div id="top-bar">
    <div class="title-text">
      <a href="/" title="Home">{{package}}</a>
    </div>
    ${self.topMenu()}
    ${self.accountInfo()}
  </div> <!-- top-bar -->
</%def>

<%def name="topMenu()">
  <div class="top-menu">
    <%self:subMenu title="Trading" url="" perm="trade">
      ${menuItem("Home",               "/",                 "view")}
    </%self:subMenu>
  </div>
</%def>

<%def name="subMenu(title, url, perm, isCurrent)">
  %if has_permission(perm, request.root, request):
    <div class="sub-menu">
      <% tabClass = " current-menu-tab" if isCurrent else "" %>
      <div class="menu-tab${tabClass}">
        %if url:
          <a href="${url}" title="">${title}</a> 
        %else:
          <span title="">${title}</span> 
        %endif
      </div>
      <div class="menu-drop-down">
        ${caller.body()}
      </div>
    </div>
  %endif
</%def>

<%def name="menuItem(title, url, perm)">
  %if has_permission(perm, request.root, request):
    <div class="menu-item">
      %if url:
        <a href="${url}" title="">${title}</a> 
      %else:
        <span title="">${title}</span> 
      %endif
    </div>
  %endif
</%def>

<%def name="accountInfo()">
<%
  currentUser = getattr(request, 'user')
%>
  <div class="account-info">
    %if currentUser:
      <div class="current-user">
        <a href="/user" title="Account Info">${currentUser.fullname}</a>
      </div>
      <div class="logout">
        <a href="/logout">Logout</a>
      </div>
    %else:
      <div class="current-user">
      </div>
      <div class="login">
        <a href="/login">Login</a>
      </div>
    %endif
  </div>
</%def>

<%def name="background()">
  <div id="background" class="grey"></div>
</%def>

<%def name="overlay()">
  <div id="overlay"></div>
</%def>

<%def name="bottomBar()">
  <div id="bottom-bar">
    <div class="bottom-left">
      <% flashMsgs = request.session.peek_flash() %>
      %if flashMsgs:
        ${flashMsgs[-1]}
      %endif
    </div>
    <div class="bottom-center">
    </div>
  </div> <!-- bottom-bar -->
</%def>

<%def name="timeOutWarning()">
  <div id="time-out-warning">
    <div id="time-out-dialog">
      <a class="close" href="#">×</a>  
      <h4>Time Out Warning</h4>
        <p>
          You've been inactive for a while. For your security, we'll log you out
          automatically. Click "Stay Online" to continue your session. 
        </p>
        <p>
          Your session will expire in <span class="secsRemaining">120</span> seconds.
        </p>
        <div class="tool-bar">
          <input type="submit" id="extendSession" value="Stay Online" />
          <input type="submit" id="logoutSession" value="Logout" />
        </div>
    </div>
  </div>

  <script type="text/javascript" charset="utf8">
    $(function () {
        initIdleTimeOut(${request.userTimeOut});
    });
  </script>
</%def>

<%namespace file="jslibs.mako" name="jslibs" />
<%def name="javascript()">
  ${jslibs.jquery()}
  ${jslibs.jquery_idletimer()}
</%def>

