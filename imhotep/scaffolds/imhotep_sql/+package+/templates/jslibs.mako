<%def name="javascript()">
  ${jquery()}
  ${jquery_ui()}
  ${jquery_idletimer()}
  ${jquery_datatables()}
</%def>

<%def name="jquery()">
  <script type="text/javascript" charset="utf8" 
          src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript" charset="utf8">
    if (typeof jQuery === 'undefined') {
        document.write('<script type="text/javascript" charset="utf8"\
                                src="/static/jquery.1.10.2.min.js">\x3C/script>');
    }

    // disable double submits on all forms as soon we have jquery available
    $(function () {
        $('form').submit(function() {
            if ($(this).data("alreadySubmitted")) {
                $('input[type=submit]', this).attr('disabled', 'disabled');
                return false;
            } else {
                $(this).data("alreadySubmitted", true);
                $("input[type=submit]", this).css("color", "GrayText");
                return true;
            }
        });
    });
  </script>
</%def>

<%def name="jquery_idletimer()">
##  <script type="text/javascript" charset="utf8" 
##          src="//cdnjs.cloudflare.com/ajax/libs/jquery-idletimer/1.0.0/idle-timer.min.js"></script>
##  <script>
##    if (typeof jQuery.idleTimer === 'undefined') {
##        document.write('<script type="text/javascript" charset="utf8"\
##                                src="/static/idle-timer.1.0.0.min.js">\x3C/script>');
##    }
##  </script>
  <script type="text/javascript" charset="utf8"
          src="/static/idle-timer.1.0.0.min.js"></script>
  <script type="text/javascript" charset="utf8" src="/static/idle_timeout.js?${request.revstr}">
  </script>
</%def>

<%def name="jquery_ui()">
  <script type="text/javascript" charset="utf8" 
          src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript" charset="utf8">
    if (typeof jQuery.ui === 'undefined') {
        document.write('<script type="text/javascript" charset="utf8"\
                                src="/static/jquery-ui.1.10.3.min.js">\x3C/script>');
    }
  </script>
</%def>

<%def name="jquery_datatables()">
##  <script type="text/javascript" charset="utf8" 
##          src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
##  <script type="text/javascript" charset="utf8">
##    if (typeof jQuery.fn.dataTable === 'undefined') {
##        document.write('<script type="text/javascript" charset="utf8"\
##                                src="/static/jquery.dataTables.1.9.4.min.js">\x3C/script>');
##    }
##  </script>
  <script type="text/javascript" charset="utf8"
          src="/static/jquery.dataTables.1.9.4.min.js"></script>
</%def>

