<%inherit file="main.mako" />

<div id="content">
  <div id="user-settings">
    <h2>User:</h2>
    <form method="POST" action="${request.path_url}" enctype="multipart/form-data">
      %if form.errors:
        <ul class="errors">
          %for field, errors in form.errors.items():
              %for error in errors:
                  <li>${form[field].label}: ${error}</li>
              %endfor
          %endfor
        </ul>
      %endif
      <div class="user">
        %for field in form:
            <div class="field">
              ${field.label} ${field(**field.extattrs)}
            </div>
        %endfor
      </div> <!-- user -->
      <div class="tool-bar">
        ${btns.csrfToken}
        ${btns.hiddenBtn}
        ${btns.cancelBtn}
        ${btns.okBtn}
        ${btns.anotherBtn}
      </div>
    </form>
  </div>
  <div class="is-required-key">
    <span class="red-star">*</span> = is required
  </div>
</div>
