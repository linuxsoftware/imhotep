<%inherit file="main.mako" />

<div id="content">
  <div class="tablets">
    <div class="tablet-outer" id="login-tablet">
      <div class="tablet" >
        <div class="tablet-text inset-text">
          Verify
        </div>
        <form action="${url}" method="post">
          <div>${message}</div>
          <div class="login">
            ${form.cameFrom}
            ${form.password(autofocus=True, maxlength=200)}
          </div>
          <div class="tool-bar">
            ${btns.csrfToken}
            ${btns.okBtn}
          </div>
        </form>
      </div> <!-- tablet -->
    </div> <!-- tablet-outer -->
  </div> <!-- tablets -->
</div> <!--content -->

<%def name="background()">
  <div id="background" class="cow"></div>
</%def>

<%def name="timeOutWarning()">
</%def>

<%def name="javascript()">
</%def>

