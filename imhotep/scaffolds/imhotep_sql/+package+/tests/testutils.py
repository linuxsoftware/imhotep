#---------------------------------------------------------------------------
# Testing infrastructure
#---------------------------------------------------------------------------
import unittest
from os import path
from pyramid import testing
from pyramid.paster import get_appsettings
from webob.multidict import MultiDict
from webtest import TestApp
import logging
from ..models.meta import DBSession
from .. import main

def neverCommit(request, response):
    return True

class DummyRequest(testing.DummyRequest):
    def __init__(self):
        testing.DummyRequest.__init__(self)
        self.POST = MultiDict(self.POST)
        self.GET  = MultiDict(self.GET)
        self.user = testing.DummyResource(loginname = 'testing',
                                          firstname = 'Test',
                                          lastname  = 'User',
                                          role_id   = 'admin')

class TestCase(unittest.TestCase):
    def assertHasAttr(self, obj, attr):
        self.assertTrue(hasattr(obj, attr),
                        'expected object %r to have attribute %r' % (obj, attr))

    def assertNoAttr(self, obj, attr):
        self.assertFalse(hasattr(obj, attr),
                         'object %r should not have attribute %r' % (obj, attr))

    def assertNearlyEqual(self, a, b, tolerance):
        self.assertTrue(b-tolerance < a < b+tolerance,
                        '%r != %r \N{plus-minus sign} %r' % (a, b, tolerance))

    def assertGreaterThan(self, a, b):
        self.assertTrue(a > b,
                        '%r \N{not greater-than} %r' % (a, b))

    def assertLessThan(self, a, b):
        self.assertTrue(a < b,
                        '%r \N{not less-than} %r' % (a, b))


class UnitTestCase(TestCase):
    def setUp(self):
        self.config = testing.setUp()
        routes.includeme(self.config)

    def tearDown(self):
        testing.tearDown()


class FuncTestCase(TestCase):
    def setUp(self):
        here =  path.abspath(path.dirname(__file__))
        settings = get_appsettings(path.join(here, 'tests.ini'))
        app = main({}, **settings)
        self.testapp = TestApp(app)

    def tearDown(self):
        DBSession.remove()
        del self.testapp
        for handler in logging.getLogger("").handlers:
            handler.flush()
            handler.close()

