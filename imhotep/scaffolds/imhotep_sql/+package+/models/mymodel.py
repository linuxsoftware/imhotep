# ------------------------------------------------------------------------------
# MyModel Resources
# ------------------------------------------------------------------------------
from sqlalchemy import (Column,
                        ForeignKey,
                        Date,
                        DateTime,
                        Integer,
                        String,
                        Text,
                        Numeric)
from sqlalchemy.orm import relationship
from .meta import Base, IntegerDefaults


class MyModel(Base):
    """An example table"""
    __tablename__ = 'mymodel'
    id              = Column(Integer, primary_key=True)
    name            = Column(Text, unique=True)
    value           = Column(Integer, **IntegerDefaults)

    def __init__(self, name, value):
        self.name = name
        self.value = value
