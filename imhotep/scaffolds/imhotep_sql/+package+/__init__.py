# ------------------------------------------------------------------------------
# 
# ------------------------------------------------------------------------------

from .utils import monkey
from pyramid.config import Configurator
from pyramid.paster import setup_logging
from pyramid.events import ApplicationCreated, subscriber

# deprecated in Pyramid 1.5, but still needed in 1.4
from pyramid.security import unauthenticated_userid

from sqlalchemy import engine_from_config
from .models.meta import DBSession, Base
from .models.user import User
from .models.settings import Settings, GlobalSetting
from .utils import getSetupInfo, secureSettings
from datetime import datetime
from pprint import pformat
import atexit
import sys
from sqlalchemy.exc import ProgrammingError
import logging
log = logging.getLogger(__name__)

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    settings['here']     = global_config.get('here', '.')
    settings['__file__'] = global_config.get('__file__', 'development.ini')
    loggersConfig = settings.get('loggersConfig')
    logging.captureWarnings(True)
    if loggersConfig:
        setup_logging(loggersConfig)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings)
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_request_method(getUser, 'user', reify=True)
    config.add_request_method(verifyUser)
    config.add_request_method(unverifyUser)
    config.scan()
    return config.make_wsgi_app()


@subscriber(ApplicationCreated)
def hello(event):
    settings = event.app.registry.settings
    log.info("Imhotep says hello\n%s", pformat(secureSettings(settings)))

@atexit.register
def goodbye():
    if log is not None:
        log.info("Shutting down at {}".format(datetime.now().isoformat(' ')))
    if logging is not None:
        for handler in logging.getLogger("").handlers:
            handler.flush()
            handler.close()

# ------------------------------------------------------------------------------
# Current User functions
# from http://docs.pylonsproject.org/projects/pyramid_cookbook/en/latest/auth/user_object.html
# ------------------------------------------------------------------------------
def getUser(request):
    name = unauthenticated_userid(request)
    if name is not None:
        #log.debug("Looking up user %s " % name)
        user = User.getByLogin(name)
        if user is not None:
            user.gauthVerified = request.session.get("userVerified", False)
        return user

def verifyUser(request):
    user = request.user
    if user is not None:
        user.gauthVerified = request.session["userVerified"] = True

def unverifyUser(request):
    request.session["userVerified"] = False
    user = request.user
    if user is not None:
        user.gauthVerified = False
