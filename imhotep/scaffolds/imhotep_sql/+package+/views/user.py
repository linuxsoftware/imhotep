#---------------------------------------------------------------------------
# User & Reset User Password Views
#---------------------------------------------------------------------------
from pyramid.httpexceptions import HTTPForbidden
from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotImplemented
from pyramid_mailer import get_mailer
from ..utils.proc import launchPostMail
from pyramid_mailer.message import Message
from pyramid.renderers import render
from pyramid.security import remember
from pyramid.view import view_config
from wtforms.fields import PasswordField
from wtforms.fields import StringField
from wtforms.fields import SelectField
from wtforms.fields import BooleanField
from wtforms.validators import Email as EmailValid
from wtforms.validators import EqualTo
from wtforms.validators import InputRequired
from wtforms.validators import Length
from wtforms.validators import ValidationError
from ..utils.formsextns import DisabledBooleanField
from ..utils.formsextns import DisabledIntegerField
from ..utils.formsextns import DisabledPasswordField
from ..utils.formsextns import DisabledRadioField
from ..utils.formsextns import DisabledStringField
from ..utils.formsextns import PyramidForm
from ..utils.formsextns import SubmitBtns
from ..utils.gauth import genKey, getSecret, getQRCode
from ..models import DBSession
from ..models import User
from ..models import UserPasswordReset
from datetime import datetime, timedelta
from pytz import common_timezones

import logging
log = logging.getLogger(__name__)
import smtplib
log.write = log.debug 
smtplib.stderr = log


class EditUserForm(PyramidForm):
    login           = DisabledStringField("Login")
    name            = StringField("Name")
    email           = StringField("Email", [EmailValid()])
    timezone        = SelectField("Time Zone",
                                  choices = [(z,z) for z in common_timezones])
    failed_logins   = DisabledIntegerField("Failed Logins")
    usesGauth       = DisabledBooleanField("Uses GAuth")

@view_config(route_name="user",
             http_cache=0,
             renderer='edit_user.mako',
             permission='edit')
def editUser(request):
    log.debug("Edit user screen for %s" % request.user)
    form = EditUserForm(request.user)
    form.cancelUrl = \
    form.okUrl     = request.route_url('home')
    #form.timezone.choices = [(zone,zone) for zone in common_timezones]
    form.btns.addBtn.label.text = "Set GAuth"
    form.addUrl = request.route_url('setgauth') 
    form.btns.modBtn.label.text = "Change Password"
    form.modUrl = request.route_url('changepwd') 
    return form.handle(request)

NewPasswordValid  = [InputRequired(), Length(min=7, max=200)]
PasswordConfirmed = [EqualTo('password', "New password doesn't match"),
                     Length(max=200)]

class ChangePwdForm(PyramidForm):
    oldPassword = PasswordField("Old Password", 
                                [InputRequired(), Length(max=200)])
    password    = PasswordField("New Password", NewPasswordValid)
    confirm     = PasswordField("Confirm New Password", PasswordConfirmed)

    def validate_oldPassword(self, field):
        if not self.obj.verifyPassword(field.data):
            log.warning("Wrong pwd for %s in change password screen" % self.obj)
            raise ValidationError("Incorrect old password")

@view_config(route_name="changepwd",
             renderer='change_pwd.mako',
             permission='edit')
def changePwd(request):
    log.debug("Change password screen for %s" % request.user)
    form = ChangePwdForm(request.user)
    form.cancelUrl = \
    form.okUrl     = request.route_url('user')
    return form.handle(request)

class ForgotPwdForm(PyramidForm):
    nameOrEmail = StringField("Login or Email",
                              [InputRequired(), Length(max=100)])

    def validate_nameOrEmail(self, field):
        user = User.getByLogin(field.data)
        if user is None:
            user = User.getByEmail(field.data)
        if user is None:
            raise ValidationError("No such user")
        if not user.email:
            raise ValidationError("User does not have email")
        self.obj = user

    def handleOk(self, request, retval):
        log.debug("ForgotPwdForm OK Button pushed")
        user = self.obj
        emailPwdResetToken(user, request)
        return HTTPFound(location = request.route_url('emailsent'))

@view_config(route_name='forgotpwd',
             renderer='forgot_pwd.mako',
             permission='view')
def forgotPassword(request):
    log.debug("Forgot Password screen")
    form = ForgotPwdForm()
    form.cancelUrl = request.route_url('login')
    return form.handle(request);

def emailPwdResetToken(user, request):
    someTime = datetime.utcnow() + timedelta(minutes=30)
    usersIP  = request.environ.get('X-Real-IP', request.remote_addr)
    reset = UserPasswordReset(user, someTime, usersIP)
    DBSession.add(reset)
    log.info("Created reset token %s for %s valid until %s" 
                  % (reset.id, usersIP, someTime))
    textContent = render('reset_pwd_email_text.mako',
                         {'token'     : reset.id,
                          'expires'   : reset.expires,
                          'login'     : user.login,
                          'name'      : user.name},
                        request)
    message = Message(subject="Password reset requested",
                      recipients=[user.email],
                      body=textContent)
    mailer = get_mailer(request)
    log.info("Queueing reset password email to %s" % user.email)
    mailer.send_to_queue(message)
    launchPostMail(request.registry.settings)

@view_config(route_name='emailsent',
             renderer='email_sent.mako',
             permission='view')
def mailSent(request):
    log.debug("Email sent screen")
    btns = SubmitBtns(request.POST)
    if request.method == 'POST':
        if btns.okBtn.data:
            return HTTPFound(location = request.route_url('login'))
    return { 'btns': btns }

class ResetPwdForm(PyramidForm):
    password    = DisabledPasswordField("New Password", NewPasswordValid)
    confirm     = DisabledPasswordField("Confirm New Password", PasswordConfirmed)

    def handleOk(self, request, retval):        
        log.debug("ResetPwdForm OK Button pushed")
        self.obj.isused = True
        self.obj.user.password = self.password.data
        headers = remember(request, self.obj.user.login.lower())
        return HTTPFound(location = request.route_url('home'),
                         headers  = headers)

@view_config(route_name='resetpwd',
             renderer='reset_pwd.mako',
             permission='view')
def resetPassword(request):
    token = request.matchdict['token']
    log.debug("Reset password screen for token %s" % token)
    info = {}
    reset = UserPasswordReset.getByToken(token)
    form = ResetPwdForm(reset)
    form.cancelUrl = request.route_url('user')
    if reset is not None:
        log.debug("Valid token for user {}".format(reset.user))
        info['validtoken']     = True
        form.password.disabled = False
        form.confirm.disabled  = False
    else:
        log.debug("Invalid token %s" % token)
        info['validtoken']       = False
        form.btns.okBtn.disabled = True
    return form.handle(request, info)

@view_config(route_name="setgauth",
             renderer='set_gauth.mako',
             permission='edit')
def setGoogleAuth(request):
    log.debug("Set gauth screen for %s" % request.user)
    user = request.user
    btns = SubmitBtns(request.POST,
                      csrfToken = request.session.get_csrf_token())
    btns.delBtn.label.text = "Delete GAuth"
    btns.delBtn.disabled = not user.usesGauth
    gauthUrl = request.route_url('gauth.png')

    if request.method == 'POST':
        if btns.csrfToken.data != request.session.get_csrf_token():
            log.info("CSRF token failed in setGoogleAuth")
            return HTTPForbidden()
        newKey = request.session.get('newgauth')
        if newKey is None:
            log.warning("No gauth key saved in session for setGoogleAuth POST")
            return HTTPForbidden()
        if btns.okBtn.data:
            log.debug("Set gauth to {}".format(newKey))
            user.gauthkey = newKey
        elif btns.delBtn.data:
            user.gauthkey = None
        elif btns.cancelBtn.data:
            pass
        elif btns.hiddenBtn.data:
            # redisplay the same secret
            secret = getSecret(newKey, user.id)
            return {'secret':    secret,
                    'gauthUrl':  gauthUrl,
                    'btns':      btns}
        else:
            return HTTPNotImplemented()                
        return HTTPFound(location = request.route_url('user'))

    newKey = genKey()
    request.session['newgauth'] = newKey
    secret = getSecret(newKey, user.id)
    return {'secret':    secret,
            'gauthUrl':  gauthUrl,
            'btns':      btns}

@view_config(route_name="gauth.png",
             renderer="png",
             permission='edit')
def qrcodeImg(request):
    newKey = request.session.get('newgauth')
    if newKey is None:
        log.info("No gauth key saved in session for gauth.png")
        return HTTPForbidden()
    return getQRCode(newKey, request.user)
