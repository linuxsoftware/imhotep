# ------------------------------------------------------------------------------
# Login/Logout Views
# ------------------------------------------------------------------------------
from pyramid.httpexceptions import HTTPNotFound
from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotImplemented
from pyramid.renderers import render, render_to_response
from pyramid.response import Response
from pyramid.security import forget
from pyramid.security import remember
from pyramid.view import forbidden_view_config
from pyramid.view import view_config
from wtforms import Form
from wtforms.fields import HiddenField
from wtforms.fields import StringField
from wtforms.fields import PasswordField
from wtforms.validators import Length
from ..utils.formsextns import SubmitBtns
from ..security import checkAuthentication, checkVerification, Authentication

import logging
log = logging.getLogger(__name__)


@forbidden_view_config()
def forbidden(request):
    log.debug("Forbidden request to {}".format(request.url))
    user = request.user
    if user is None:
        return login(request)
    elif user.usesGauth and not user.gauthVerified:
        return verifyUser(request)
    else:
        log.debug("Forbidden user is {}".format(request.user.login))
        # return 404 NotFound instead of 403 Forbidden, so we are not leaking
        # the information that there even is a secure page there
        return HTTPNotFound(request.path)

class LoginForm(Form):
    cameFrom = HiddenField()
    login    = StringField(validators=[Length(max=200)])
    password = PasswordField(validators=[Length(max=200)])

class VerifyForm(Form):
    cameFrom = HiddenField()
    password = PasswordField(validators=[Length(max=200)])

@view_config(route_name='login')
def login(request):
    log.debug("Login screen")
    loginUrl = request.route_url('login')
    referrer = request.url
    if referrer == loginUrl:
        referrer = '/' # never use the login form itself as came_from
    btns = SubmitBtns(request.POST,
                      csrfToken = request.session.get_csrf_token())
    values = {'message':    '',
              'url':        loginUrl,
              'btns':       btns}
    form = LoginForm(request.POST, cameFrom = referrer)
    if request.method == 'POST':
        login    = form.login.data.lower()
        password = form.password.data
        if btns.csrfToken.data != request.session.get_csrf_token():
            log.info("CSRF token failed in login")
            password = None
        if btns.loginBtn.data:
            result, user = checkAuthentication(login, password)
            if result in (Authentication.OK, Authentication.TO_VERIFY):
                headers = remember(request, login)
                if result == Authentication.OK:
                    return HTTPFound(location = form.cameFrom.data,
                                     headers  = headers)
                else: # result == Authentication.TO_VERIFY:
                    values["form"] = VerifyForm(request.POST)
                    return Response(render("verify.mako", values, request),
                                    headers = headers)
            elif result == Authentication.LOCKED_OUT:
                values["message"] = "Too many attempts, failed login.  "    \
                                    "Contact the webmaster to " \
                                    "have your account reset."
            else:
                values["message"] = "Failed login"
        else:
            return HTTPNotImplemented()                
    btns.csrfToken.data = request.session.new_csrf_token()
    values['form'] = form
    return render_to_response('login.mako', values, request)

@view_config(route_name='login', userNeedsVerification=True)
@view_config(route_name='home',  userNeedsVerification=True)
def verifyUser(request):
    # for 2 factor authentication
    log.debug("Verify User screen")
    loginUrl = request.route_url('login')
    referrer = request.url
    if referrer == loginUrl:
        referrer = '/' # never use the login form itself as came_from
    btns = SubmitBtns(request.POST,
                      csrfToken = request.session.get_csrf_token())
    form = VerifyForm(request.POST, cameFrom = referrer)
    values = {'message':    '',
              'form':       form,
              'url':        loginUrl,
              'btns':       btns}
    if request.method == 'POST':
        givenOtp = form.password.data
        if btns.csrfToken.data != request.session.get_csrf_token():
            log.info("CSRF token failed in verifyUser")
            givenOtp = None
        if btns.okBtn.data:
            result = checkVerification(request.user, givenOtp)
            if result == Authentication.OK:
                log.info("{} verified OK".format(request.user))
                request.verifyUser()
                return HTTPFound(location = form.cameFrom.data)
            elif result == Authentication.LOCKED_OUT:
                values['message'] = 'Too many attempts, failed verification'
            else:
                values['message'] = 'Failed verification'
        else:
            return HTTPNotImplemented()                
    return render_to_response('verify.mako', values, request)

@view_config(route_name='logout')
def logout(request):
    log.debug("Logout screen")
    headers = forget(request)
    request.unverifyUser()
    return HTTPFound(location = request.route_url('home'),
                     headers = headers)
   
@view_config(route_name="keepalive.json",
             xhr=True,
             renderer='json',
             permission='access')
def getKeepAlive(request):
    retval = {'keepAlive':  "I am still alive"}
    return retval    
