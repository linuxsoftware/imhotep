# ------------------------------------------------------------------------------
# Security Central
# ------------------------------------------------------------------------------
from .models import User
from pyramid.security import Allow, Everyone, Authenticated, ALL_PERMISSIONS
from pyramid.authentication import SessionAuthenticationPolicy
from pyramid.authorization  import ACLAuthorizationPolicy
from .utils.gauth import getSecret, verifyOneTimePassword

import logging
log = logging.getLogger(__name__)


# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------
def includeme(config):
    """Set up the authentication and authorization policies"""
    authnPolicy = SessionAuthenticationPolicy(callback=getGroups)
    authzPolicy = ACLAuthorizationPolicy()
    config.set_authentication_policy(authnPolicy)
    config.set_authorization_policy(authzPolicy)
    config.set_root_factory(Root)

    # Custom predicates
    config.add_view_predicate("userNeedsVerification",
                              UserNeedsVerificationPredicate)
    log.info("security set up")



#TODO: Look at changing from beaker sessions to pyramid_redis_sessions
#
#This commment is from 
#https://groups.google.com/forum/#!topic/pylons-discuss/9zDnLK15Wks
#
#Beaker is not maintained anymore. The development version of Pyramid 
#1.5 is recommending pyramid_redis_sessions instead, which is the only 
#fully-maintained backend at this time. I was unhappy with this because 
#I was about to put an application into production that's using Beaker 
#file-based sessions (which I've never had problems with). But later we 
#decided we might use Redis on a wider scope, for caching and usage 
#statistics, so pyramid_redis_sessions would be a good way to start 
#gaining experience with it. I installed it last week, using a 
#self-compiled Redis, and it worked flawlessly. I'm going to put it in 
#production next week. I'm still using PostgreSQL as the primary 
#database. 
#Longer-term, somebody needs to write a replacement for Beaker, or 
#'pyramid_*' packages for the various backends. I've started looking 
#into it but I can't commit to it right now. The development version of 
#Pyramid has gotten more building blocks to write such backends with. 
#We also need some HOWTOs on setting up non-backend sessions; e.g., 
#with regular database tables. I started trying that but got bogged 
#down in creating and managing the session ID, which Beaker does but 
#Pyramid doesn't support internally. I needed session IDs not only to 
#link the data to the user, but also to log with the request to count 
#the sessions per day, their length and entry/exit pages. It was enough 
#extra work that I went back to Beaker. 
#The fundamental problem with Beaker is these lock files. It's using an 
#obsolete protocol borrowed from Perl::Mason that's more hassle than 
#it's worth. Beaker's successor, Dogpile, doesn't use this kind of lock 
#file. But Dogpile's author is not recommending it for sessions, which 
#I didn't expect. In any case, there's no Pyramid adapter yet for 
#Dogpile, that does the backend-neutral configuration like 
#pyramid_beaker does, so it's not an option without that.

# ------------------------------------------------------------------------------
# Authentication
# ------------------------------------------------------------------------------
def getGroups(name, request):
    user = request.user
    if user is None:
        log.info("getGroups called for non-existant user %s" % name)
        return None
    if user.usesGauth and not user.gauthVerified:
        log.debug("getGroups called for non-verified user %s" % name)
        return None
    return getGroupsForUser(user)

def getGroupsForUser(user):
    groups = []
    return groups

class Authentication:
    TO_VERIFY, OK, FAILED, LOCKED_OUT = range(4)

def checkAuthentication(name, givenPass):
    """Check the given login and password matches an active user"""
    result = Authentication.FAILED
    name = name.replace(':', ';')
    user = User.getByLogin(name)
    if user is not None:
        if user.failedlogins < 99:
            if givenPass and user.verifyPassword(givenPass):
                log.info("User %s password OK" % name)
                if user.usesGauth: 
                    user.gauthVerified = False
                    result = Authentication.TO_VERIFY
                else:
                    result = Authentication.OK
                    user.failedlogins = 0
            else:
                log.info("User %s authentication FAILED" % name)
                user.failedlogins += 1
        else:
            log.warning("User %s locked out" % name)
            result = Authentication.LOCKED_OUT
    else:
        log.info("User %s does not exist" % name)

    return result, user

def checkVerification(user, givenOtp):
    """Verify the given one-time-password of users who use gauth"""
    result = Authentication.FAILED
    if user.usesGauth:
        if user.failedlogins < 3:
            secret = getSecret(user.gauthkey, user.id)
            if givenOtp and verifyOneTimePassword(givenOtp, secret):
                log.info("User %s verification OK" % user.login)
                result = Authentication.OK
                user.failedlogins = 0
            else:
                log.info("User %s verification FAILED" % user.login)
                user.failedlogins += 1
        else:
            log.warning("User %s locked out" % user.login)
            result = Authentication.LOCKED_OUT
    else:
        log.error("User %s does not use gauth!!!" % user.login)

    return result


# ------------------------------------------------------------------------------
# View Predicates
# ------------------------------------------------------------------------------
class UserNeedsVerificationPredicate(object):
    def __init__(self, flag, config):
        self.flag = flag

    def text(self):
        if self.flag:
            return "User does need verification"
        else:
            return "User does not need verification"
    phash = text

    def __call__(self, context, request):
        user = request.user
        needsVerification = user and user.usesGauth and not user.gauthVerified
        return self.flag == needsVerification

# ------------------------------------------------------------------------------
# Root Security Domain / Authorization
# ------------------------------------------------------------------------------
class Root(dict):
    """The root security domain"""
    __acl__ = [(Allow, Everyone,         ()),
               (Allow, Authenticated,   ('view', 'access')),
               (Allow, 'role:manager',  ('manage','edit')),
               (Allow, 'role:editor',   ('edit')),
               (Allow, 'role:admin',     'admin'),
               (Allow, 'role:admin',     ALL_PERMISSIONS) ]

    def __init__(self, request):
        pass

