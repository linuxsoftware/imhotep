#---------------------------------------------------------------------------
# Various utility functions
#---------------------------------------------------------------------------
from .misc import getSetupInfo, getGitTag, secureSettings, recsToRows
from .dt   import localNow, localToday, localDateTimeStr, localDateStr
from .proc import daemonSpawn, launchPostMail, getPostMailLauncher

