#---------------------------------------------------------------------------
# Monkey Patching
#---------------------------------------------------------------------------

# monkey patch required for ext.sqlalchemy.orm.model_form
# TODO move to WTForms-Alchemy or Sprox or something else
from wtforms import validators
validators.Required = validators.InputRequired

# FIXME: Monkey patches make me sad :-(
# Needed for file uploads
# clues: https://github.com/defnull/bottle/issues/382
#        http://bugs.python.org/issue19097
from cgi import FieldStorage
FieldStorage.__bool__ = lambda self: bool(self.list) or bool(self.file)

# monkey patching decimal for speed XXX does not work with simplejson renderer
#import sys
#import cdecimal
#sys.modules["decimal"] = cdecimal



