#---------------------------------------------------------------------------
# Process functions
#---------------------------------------------------------------------------
import sys
import time
import os
from os import path
import resource		# Resource usage information.

def daemonSpawn(cmd, *args, cwd="/", log="/dev/null", delayStart=0):
    pid = os.fork()
    if pid > 0:
        return os.waitpid(pid, 0)
    # Child
    os.setsid()
    try:
        pid = os.fork()
        if pid > 0:
            os._exit(0)
    except OSError as e:
        sys.stderr.write("Fork #2 failed: {}\n".format(e))
        os._exit(1)
    # Grandchild
    os.chdir(cwd)
    os.umask(0)
    sys.stdout.flush()
    sys.stderr.flush()
    maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
    if (maxfd == resource.RLIM_INFINITY):
        maxfd = 2048
    for fd in range(0, maxfd):
        try:
            os.close(fd)
        except OSError:	# fd wasn't open to begin with (ignored)
            pass
    os.open("/dev/null", os.O_RDONLY)                      # reopen STDIN
    os.open(log, os.O_RDWR|os.O_CREAT|os.O_APPEND, 0o660)  # reopen STDOUT
    os.dup2(1, 2)                                          # reopen STDERR
    if delayStart:
        time.sleep(delayStart) # favour parent process
    os.execlp(cmd, path.basename(cmd), *args)

def launchPostMail(settings):
    here    = settings['here']
    cfgfile = settings['__file__']
    daemonSpawn("taurus_post_mail", "data/mail", "--config", cfgfile,
                cwd=here, log="post_mail.log", delayStart=1.25)

def getPostMailLauncher(settings):
    return lambda: launchPostMail(settings)

